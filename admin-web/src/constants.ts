export const baseURL =
  "https://my-json-server.typicode.com/seanlim/example-project";

export const tags = [
  "Career Oppourtunities",
  "Internships",
  "Fintech",
  "Consulting",
  "Finance",
  "Healthcare",
  "AI",
  "Analytics",
];
