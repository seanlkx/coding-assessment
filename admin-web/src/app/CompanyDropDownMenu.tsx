import {
  DropdownMenu,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuContent,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu";
import { Company } from "@/types";
import EditCompanyDialog from "@/components/EditCompanyDialog";
import { Button } from "@/components/ui/button";
import { baseURL } from "@/constants";
import axios from "axios";
import { MoreHorizontal } from "lucide-react";
import { useToast } from "@/components/ui/use-toast";

export default function CompanyDropDownMenu({
  company,
}: {
  company: Company;
}): JSX.Element | null {
  const { toast } = useToast();
  return (
    <DropdownMenu>
      <DropdownMenuTrigger asChild>
        <Button variant="ghost" className="h-8 w-8 p-0">
          <span className="sr-only">Open menu</span>
          <MoreHorizontal className="h-4 w-4" />
        </Button>
      </DropdownMenuTrigger>
      <DropdownMenuContent align="end">
        <DropdownMenuLabel>Actions</DropdownMenuLabel>
        <EditCompanyDialog company={company} />
        <DropdownMenuItem
          className="text-red-600"
          onClick={() =>
            axios
              .delete(`${baseURL}/companies/${company.id}`)
              .then((response) => {
                toast({
                  title: "DELETE Success",
                  description: `Mock API returned ${response.status}`,
                });
              })
              .catch((err) => {
                toast({
                  title: "DELETE Failed",
                  description: err,
                  variant: "destructive",
                });
              })
          }
        >
          Delete
        </DropdownMenuItem>
        <DropdownMenuItem
          className="text-red-600"
          onClick={() =>
            axios
              .delete(`${baseURL}/companies/-1`)
              .then((response) => {
                toast({
                  title: "DELETE Success",
                  description: `Mock API returned ${response.status}`,
                });
              })
              .catch((err) => {
                toast({
                  title: "DELETE Failed",
                  description: err.message,
                  variant: "destructive",
                });
              })
          }
        >
          Delete (Erroneous for demo purposes)
        </DropdownMenuItem>
      </DropdownMenuContent>
    </DropdownMenu>
  );
}
