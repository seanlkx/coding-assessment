"use client";

import CreateCompanyDialog from "@/components/CreateCompanyDialog";
import { DataTable } from "@/components/DataTable";
import EditCompanyDialog from "@/components/EditCompanyDialog";
import { Badge } from "@/components/ui/badge";
import { Button } from "@/components/ui/button";
import {
  DropdownMenu,
  DropdownMenuSeparator,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuContent,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu";
import { Toaster } from "@/components/ui/toaster";
import { useToast } from "@/components/ui/use-toast";
import { baseURL } from "@/constants";
import { Company } from "@/types";
import { ColumnDef } from "@tanstack/react-table";
import axios from "axios";
import { MoreHorizontal } from "lucide-react";
import { useEffect, useState } from "react";
import CompanyDropDownMenu from "./CompanyDropDownMenu";

export default function Home() {
  const [companies, setCompanies] = useState<Company[]>([]);
  const { toast } = useToast();

  const columns: ColumnDef<Company>[] = [
    {
      accessorKey: "name",
      header: "Name",
      size: 3,
    },
    {
      accessorKey: "address",
      header: "Address",
    },
    {
      accessorKey: "uen",
      header: "UEN",
    },
    {
      accessorKey: "contactEmail",
      header: "Email",
    },
    {
      accessorKey: "tags",
      header: "Tags",
      cell: ({ row }) => {
        const tags: string[] = row.getValue("tags");
        return (
          <div>
            {tags.map((t) => (
              <Badge variant="outline" key={t}>
                {t}
              </Badge>
            ))}
          </div>
        );
      },
    },
    {
      id: "actions",
      cell: ({ row }) => {
        const company = row.original;
        return <CompanyDropDownMenu company={company} />;
      },
    },
  ];

  useEffect(() => {
    axios.get(`${baseURL}/companies`).then((response) => {
      setCompanies(response.data);
    });
  }, []);

  if (!companies) return null;
  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
      <h3 className="font-mono">Admin Dashboard Demo</h3>
      <div className="w-full">
        <CreateCompanyDialog />
        <DataTable columns={columns} data={companies} />
      </div>
      <Toaster />
    </main>
  );
}
