"use client";

import React from "react";
import { Dialog, DialogContent, DialogTrigger } from "@/components/ui/dialog";
import { Company } from "@/types";
import { updateCompanyFormSchema } from "@/utils/zod";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { z } from "zod";
import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "./ui/form";
import { Button } from "./ui/button";
import { Input } from "./ui/input";
import { useToast } from "./ui/use-toast";
import axios from "axios";
import { baseURL, tags } from "@/constants";
import { Checkbox } from "./ui/checkbox";
import { Textarea } from "./ui/textarea";

export default function EditCompanyDialog({
  company,
}: {
  company: Company;
}): JSX.Element {
  const { toast } = useToast();
  const form = useForm<z.infer<typeof updateCompanyFormSchema>>({
    resolver: zodResolver(updateCompanyFormSchema),
    defaultValues: { ...company },
  });

  async function onSubmit(values: z.infer<typeof updateCompanyFormSchema>) {
    const res = await axios.putForm(`${baseURL}/companies/${company.id}`, {
      ...values,
    });

    toast({ title: "Done", description: res.status });
  }

  return (
    <Dialog>
      <Form {...form}>
        <DialogContent
          className={"lg:max-w-screen-lg overflow-y-scroll max-h-[90vh]"}
        >
          <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-8">
            <FormField
              name="name"
              control={form.control}
              render={({ field }) => (
                <FormItem>
                  <FormLabel>Name</FormLabel>
                  <FormControl>
                    <Input placeholder="name" {...field} />
                  </FormControl>
                  <FormDescription>Name of the company.</FormDescription>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              name="about"
              control={form.control}
              render={({ field }) => (
                <FormItem>
                  <FormLabel>About</FormLabel>
                  <FormControl>
                    <Textarea placeholder="About" {...field} />
                  </FormControl>
                  <FormDescription>About the company.</FormDescription>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              name="address"
              control={form.control}
              render={({ field }) => (
                <FormItem>
                  <FormLabel>Address</FormLabel>
                  <FormControl>
                    <Input placeholder="Address" {...field} />
                  </FormControl>
                  <FormDescription>Address of the company.</FormDescription>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              name="uen"
              control={form.control}
              render={({ field }) => (
                <FormItem>
                  <FormLabel>Address</FormLabel>
                  <FormControl>
                    <Input placeholder="UEN" {...field} />
                  </FormControl>
                  <FormDescription>UEN of the company.</FormDescription>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              name="contactEmail"
              control={form.control}
              render={({ field }) => (
                <FormItem>
                  <FormLabel>Email</FormLabel>
                  <FormControl>
                    <Input placeholder="Email" {...field} />
                  </FormControl>
                  <FormDescription>Email of the company.</FormDescription>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormItem>
              <FormLabel>Tags</FormLabel>
              {tags.map((t) => (
                <FormField
                  name="tags"
                  control={form.control}
                  key={t}
                  render={({ field }) => (
                    <FormItem
                      key={t}
                      className="flex flex-row items-start space-x-3 space-y-0"
                    >
                      <FormControl>
                        <Checkbox
                          checked={field.value?.includes(t)}
                          onCheckedChange={(checked) => {
                            return checked
                              ? field.onChange([...field.value, t])
                              : field.onChange(
                                  field.value?.filter((value) => value !== t)
                                );
                          }}
                        />
                      </FormControl>
                      <FormLabel className="pl-1 font-normal">{t}</FormLabel>
                    </FormItem>
                  )}
                />
              ))}
            </FormItem>
            <Button type="submit">Submit</Button>
          </form>
        </DialogContent>
      </Form>
      <DialogTrigger className="pl-2 text-base font-xs">Edit</DialogTrigger>
    </Dialog>
  );
}
