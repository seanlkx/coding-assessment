export type Company = {
  id: string;
  name: string;
  about: string;
  address: string;
  uen: string; // unique
  contactEmail: string;
  tags: string[];
};

export type Article = {
  id: string;
  title: string;
  content: string;
  companyID: string; // Not gonna enforce constraints here
};
