"use client";

import { z } from "zod";

export const updateCompanyFormSchema = z.object({
  name: z.string(),
  about: z.string(),
  address: z.string(),
  uen: z.string(), // unique
  contactEmail: z.string(),
  tags: z.array(z.string()),
});
export const companyFormSchema = z.object({
  name: z.string(),
  about: z.string(),
  address: z.string(),
  uen: z.string(), // unique
  contactEmail: z.string(),
  tags: z.array(z.string()),
});
